from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student
from students.utils import format_records
from webargs import fields


def index(request):
    return render(request=request, template_name="index.html", context={"param": [i for i in range(100)]})


def hello(request):
    return HttpResponse("Hello World!")


def get_students(request):
    students = Student.objects.all()
    return render(
        request=request,
        template_name="students_list.html",
        context={
            "students": students,
        },
    )


def search_students(request):
    students = Student.objects.all()
    search_text = request.GET.get("search")
    search_fields = ["first_name", "last_name", "email"]
    or_filter = Q()
    for field in search_fields:
        or_filter = or_filter | Q(**{f"{field}__contains": search_text})
    students = students.filter(or_filter)

    return render(request=request, template_name="search_students.html", context={"students": students})


@csrf_exempt
def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))

    elif request.method == "GET":
        form = StudentForm()

    return render(request=request, template_name="students_create.html", context={"form": form})


@csrf_exempt
def update_student(request, pk):

    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))

    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request=request, template_name="students_update.html", context={"form": form})


@csrf_exempt
def delete_student(request, pk):
    student = get_object_or_404(Student, pk=pk)
    student.delete()

    return HttpResponseRedirect(reverse("students:get_students"))
