from django.core.exceptions import ValidationError


def first_name_validator(first_name):
    if "elon" in first_name.lower():
        raise ValidationError("This name is forbidden")
