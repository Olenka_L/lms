from django.contrib import admin
from django.urls import path
from students.views import hello, get_students, create_student, update_student, delete_student, search_students

app_name = "students"

urlpatterns = [
    path("hello/", hello, name="hello"),
    path("", get_students, name="get_students"),
    path("create/", create_student, name="create_student"),
    path("update/<int:pk>/", update_student, name="update_student"),
    path("delete/<int:pk>/", delete_student, name="delete_student"),
    path("search", search_students, name="search_students"),
]
