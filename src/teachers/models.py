import random
from django.db import models
import datetime
from faker import Faker


class Teacher(models.Model):
    first_name = models.CharField(max_length=128, null=True)
    last_name = models.CharField(max_length=128, null=True)
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)
    subject = models.CharField(max_length=128, null=True)
    grade = models.SmallIntegerField(default=0, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.id}"

    def age(self):
        return datetime.datetime.now().year - self.birthdate.year

    def generate_teachers(self, count):
        faker = Faker()
        for _ in range(count):
            Teacher.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(start_date="-30y", end_date="-18y"),
                subject=random.choice(
                    [
                        "Computer Science",
                        "Math",
                        "Electrical Engineering",
                        "Mechanical Engineering",
                        "Ecology",
                        "Geology",
                    ]
                ),
                grade=random.randint(0, 100),
            )
