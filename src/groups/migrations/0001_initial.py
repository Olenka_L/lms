# Generated by Django 4.0.2 on 2022-06-23 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Group",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(max_length=128)),
                ("course", models.SmallIntegerField(default=0, null=True)),
                ("specialization", models.CharField(max_length=128, null=True)),
                ("curator", models.CharField(max_length=128, null=True)),
                ("rating", models.SmallIntegerField(default=0, null=True)),
                ("amount_of_students", models.SmallIntegerField(default=0, null=True)),
            ],
        ),
    ]
