import random
from django.db import models
from faker import Faker


class Group(models.Model):
    name = models.CharField(max_length=128)
    course = models.SmallIntegerField(default=0, null=True)
    specialization = models.CharField(max_length=128, null=True)
    curator = models.CharField(max_length=128, null=True)
    rating = models.SmallIntegerField(default=0, null=True)
    amount_of_students = models.SmallIntegerField(default=0, null=True)

    def __str__(self):
        return f"{self.name} {self.course} {self.id}"

    def generate_groups(self, count):
        faker = Faker()
        for _ in range(count):
            Group.objects.create(
                name="Group " + str(random.randint(1, 50)),
                course=random.randint(0, 5),
                specialization=random.choice(
                    [
                        "Computer Science",
                        "Math",
                        "Electrical Engineering",
                        "Mechanical Engineering",
                        "Ecology",
                        "Geology",
                    ]
                ),
                curator=faker.last_name(),
                rating=random.randint(0, 100),
                amount_of_students=random.randint(15, 30),
            )
