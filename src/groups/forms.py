from django.core.exceptions import ValidationError
from django.forms import ModelForm

from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ("name", "course", "curator")

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_name(self):
        return self.normalize_text(self.cleaned_data["name"])

    def clean_curator(self):
        return self.normalize_text(self.cleaned_data["curator"])

    def clean(self):
        cleaned_data = super().clean()
        # extend parent method,  not overwrite

        name = cleaned_data["name"]
        curator = cleaned_data["curator"]

        if name == curator:
            raise ValidationError("Name and curator can't be equal")

        return cleaned_data
