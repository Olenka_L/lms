from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs.djangoparser import use_args

from groups.forms import GroupForm
from groups.models import Group
from groups.utils import format_records
from webargs import fields


@use_args({"name": fields.Str(required=False), "search_text": fields.Str(required=False)}, location="query")
def get_groups(request, parameters):
    form = """
        <form>
            <label>Name:</label><br>
            <input type="text" name="name"><br>

            <label>Search :</label><br>
            <input type="text" name="search_text" placeholder="Enter text to search"><br><br>

            <input type="submit" value="Submit"/>

        </form>
    """
    groups = Group.objects.all()

    search_fields = ["name", "course", "curator"]

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter = or_filter | Q(**{f"{field}__contains": param_value})
                groups = groups.filter(or_filter)
            else:
                groups = groups.filter(**{param_name: param_value})
    result = format_records(groups)

    response = form + result
    return HttpResponse(response)


@csrf_exempt
def create_group(request):
    if request.method == "POST":
        form = GroupForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_groups"))

    elif request.method == "GET":
        form = GroupForm()

    form_html = f"""
        <form method='POST'>
            {form.as_p()}
            <p><input type="submit" value="Create"/></p>
        </form>
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_group(request, pk):

    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_groups"))

    elif request.method == "GET":
        form = GroupForm(instance=group)

    form_html = f"""
          <form method='POST'>
              {form.as_p()}
              <p><input type="submit" value="Update"/></p>
          </form>
      """
    return HttpResponse(form_html)


@csrf_exempt
def delete_group(request, pk):
    group = get_object_or_404(Group, pk=pk)
    group.delete()

    return HttpResponseRedirect(reverse("groups:get_groups"))
